# dotfiles - stow

stow -S base
stow -S terminals / if needed (currently alacritty and kitty)

## Neovim

Neovim mostly lua and incompatible with vim since long time.
Legacy vimrc provided that is not to disimmiler but diverging.
Store nvim and other binaries in /usr/local/bin

## Tmux

Simple config ripped from someone, with a nice colorscheme and sensible bindings.
scripts;
- tmuxdef            creates new tmux session different bewteen server and non-server
- tmuxcode appdir    creates two new panes around a horizontal split and changes dir to the coding dir specified


## Bash / Profile

Currently;
* .bashrc for everything
* .bash_profile calls .bashrc

## $HOME/bin

Currently home to scripts of my making
