let mapleader="\<Space>"
let maplocalleader=","
" Install vim-plug if it isn't already. Needs curl"{{{
if has("nvim")
	" linux nvim
	let s:ml_path='~/.config/nvim/autoload/plug.vim'
else
	" linux vim
	let s:ml_path='~/.vim/autoload/plug.vim'
endif
if empty(glob(s:ml_path))
	exec "!curl -fLo " . s:ml_path . " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
autocmd VimEnter * PlugInstall | source $MYVIMRC
endif
"}}}
" Load Plugins with vim-plug {{{
call plug#begin('~/.vim/bundle')

" #### Completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" #### panels / bars / windows / gutters
Plug 'ap/vim-buftabline'
Plug 'tpope/vim-commentary'			" gc{motion} or visual select and gc
" #### Apearence
Plug 'kyazdani42/nvim-web-devicons'
Plug 'gruvbox-community/gruvbox'
" #### Movement
Plug 'zirrostig/vim-schlepp'
Plug 'tpope/vim-repeat'
Plug 'rhysd/clever-f.vim'
Plug 'justinmk/vim-sneak'


" #### Manipulation
Plug 'tpope/vim-surround' 			" yssb ysiwb csb} ds}

" #### Git
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

" #### Other
Plug 'tpope/vim-obsession'
Plug 'Shougo/unite.vim'
Plug 'justinmk/vim-gtfo'				" Opens terminal or filemanager
Plug 'qpkorr/vim-renamer'
Plug 'roxma/vim-tmux-clipboard'
Plug 'christoomey/vim-tmux-navigator'
Plug 'vimwiki/vimwiki'
call plug#end()
"}}}
" Color Schemes {{{

" THEMES BELOW, USE AT LEAST ONE.
if has("termguicolors")
	" hmm very much needed still
	set termguicolors
	" still needed for vim in tmux, dunno why dunno what it is, magic, hate it.
	let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
	let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
set background=dark
let g:gruvbox_invert_selection=0
colorscheme gruvbox
nnoremap <F5> :call BgToggleVar()<CR>

"}}}
" Set {{{
" [ handled by vim-plug
" filetype plugin indent on " load filetype-specific indent files
" syntax enable
" ] handled by vim-plug

" #### Visual **
set belloff=all      " nvim default is belloff, vim is bellon
set number        " always show line numbers
set relativenumber
set cursorline      " highlight current line
set lazyredraw      " redraw only when we need to
set scrolloff=8
set cmdheight=2

" Buffers
set hidden
set nobackup
set noswapfile
set updatetime=300
set nowrap          " don't wrap lines
set shortmess+=c " prevent hit enter messages

" #### tabs, tabs for indentation, spaces for alignment
set noexpandtab
set tabstop=4       " a tab is four spaces
set softtabstop=4	" number of spaces <tab> counts for when editing
set shiftwidth=4    " number of spaces to use for autoindenting
set shiftround      " use multiple of shiftwidth when indenting with '<' and '>'
set pastetoggle=<F3>

" #### Search
set showmatch       " set show matching parenthesis
set ignorecase      " ignore case when searching
set smartcase       " ignore case if search pattern is all lowercase,

set spelllang=en_gb
set spellsuggest=10

set foldmethod=marker
set foldnestmax=1

set clipboard+=unnamedplus

" set sessionoptions+=globals
" auto-session recommended
set sessionoptions+=resize,winpos,terminal


" nvim defaults that we need in vim
if !has("nvim")
	set showcmd
    set wildmenu
	set encoding=utf-8
	set hlsearch        " highlight search hlsearch/nohlsearch
	set incsearch       " search as characters are entered
	set autoread
	set autoindent
	set backspace=indent,eol,start  " Makes backspace key more powerful.
	set ttyfast
endif

" fonts, multi-platform
if has("gui_running")
	set lines=40
    set columns=120
  if has("gui_gtk2") || has("gui_gtk3")
	set guifont=Noto\ Mono\ 11
  elseif has("gui_photon")
    set guifont=Courier\ New:s11
  elseif has("gui_kde")
    set guifont=Courier\ New/11/-1/5/50/0/0/0/1/0
  elseif has("x11")
    set guifont=-*-courier-medium-r-normal-*-*-180-*-*-m-*-*
  else
    set guifont=Lucida_Console:h12:cANSI"
  endif
  " ## other GUI only options
  " remove toolbars
  set guioptions-=t
  set guioptions-=T
  " remove menu
  set guioptions-=m
  " remove scroll bars
  set guioptions-=rL
  " use console confirm instead of dialog
  set guioptions+=c
  " stop cursor blinking
  set guicursor+=a:blinkon0
endif

" In ONI diable statusbar as we have a gui one.
if exists("g:gui_oni")
	set noshowmode
	set noruler
	set laststatus=0
	set noshowcmd
	set mouse=a
endif



" }}}
" Key Remaps"{{{
nnoremap ; :
nnoremap : ;
vnoremap ; :
vnoremap : ;
nnoremap <BS> :noh<CR>
nnoremap <F8> :bd<CR>
nnoremap <leader>s :setlocal nospell!<CR>
" move through windows and tmux panes (with tmux-navigator)
nnoremap <silent> <C-k> :wincmd k<CR>
nnoremap <silent> <C-j> :wincmd j<CR>
nnoremap <silent> <C-h> :wincmd h<CR>
nnoremap <silent> <C-l> :wincmd l<CR>
imap jk <ESC>
imap <C-h> <Left>
imap <C-l> <Right>
nmap <Up> [[zz
nmap <Down> ]]zz
" make Y behave like C and D
nnoremap Y y$
" keep cursor is same place during next/prev join
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z

" buffer navigation
" now in tabline section

" this file!
nnoremap <leader>vr :source $MYVIMRC<CR>
nnoremap <leader>ve :e $MYVIMRC<CR>

" replace all
nnoremap <leader>ra yiw:%s/\<<C-r>"\>//g<left><left>
" replace all with confirm
nnoremap <leader>rc yiw:%s/\<<C-r>"\>//gc<left><left><left>
" elevate to root and save
command! W :w !sudo tee %
" easier to reach Start of text and end of line
nnoremap H ^
nnoremap L $
nnoremap M `
" no matter what we have set, we can toggle with these keys
nnoremap <bar> :setlocal cursorcolumn!<CR>
nnoremap _ :setlocal cursorline!<CR>
" faster macroing
nnoremap @ q
nnoremap q @
"}}}

" PLUGIN CONFIGURATION
" vim-sneak {{{
let g:sneak#label = 1
"}}}
" gtfo  got(terminal) gof(file explorer) {{{
let g:gtfo#terminals = { 'unix': 'alacritty --working-directory $pwd' }
" let g:gtfo#terminals = { 'unix': 'konsole --new-tab --workdir' }
" }}}
" schlepp, moves visual blocks {{{
vmap <up>    <Plug>SchleppUp
vmap <down>  <Plug>SchleppDown
vmap <left>  <Plug>SchleppLeft
vmap <right> <Plug>SchleppRight
"}}}
" Tabline and Statusline {{{ 
" # Buffer Navigation #
" normal maps
" nnoremap <C-n> :bn<CR>
" nnoremap <C-p> :bp<CR>
" inoremap <C-n> <ESC>:bn<CR>
" inoremap <C-p> <ESC>:bp<CR>
" bufferline binds, because reordering breaks inbuilt
nnoremap <silent><C-n> :BufferLineCycleNext<CR>
nnoremap <silent><C-p> :BufferLineCyclePrev<CR>
inoremap <silent><C-n> <ESC>:BufferLineCycleNext<CR>
inoremap <silent><C-p> <ESC>:BufferLineCyclePrev<CR>
" These commands will move the current buffer backwards or forwards in the bufferline
nnoremap <silent>gbl :BufferLineMoveNext<CR>
nnoremap <silent>gbh :BufferLineMovePrev<CR>

" replaces all bars with my own status lines, not pretty, but MINE.
if !exists("g:gui_oni")
    au InsertEnter * call SaveStatusLine()
    au InsertLeave * call RestoreStatusLine()
    set laststatus=2
    set statusline=
    set statusline+=%f%m%r%h%w
    set statusline+=%{fugitive#statusline()}      " [Git(master)]
    set statusline+=%=                            " right align
    set statusline+=%y
    set statusline+=[%{&fileformat}]               " file format
    set statusline+=[%{strlen(&fenc)?&fenc:&enc}] " encoding
    set statusline+=\ %p%%\ C:%v
    function! SaveStatusLine()
        let g:slBg = synIDattr(hlID("StatusLine"), "bg#")
        let g:slFg = synIDattr(hlID("StatusLine"), "fg#")
        let r = synIDattr(hlID("StatusLine"), "reverse")
        let i = synIDattr(hlID("StatusLine"), "inverse")
        if r==1 || i==1 " handle themes that use reverse
            hi StatusLine guifg=#33AA44 guibg=#000000
        else
            hi StatusLine guibg=#33AA44 guifg=#000000
        endif
        return ''
    endfunction
    function! RestoreStatusLine()
        exec 'hi StatusLine guibg=' . g:slBg . ' guifg=' . g:slFg
    endfunction
    function! LightLineFugitive()
        return exists('*fugitive#head') ? fugitive#head() : ''
    endfunction
    let g:buftabline_indicators = 1
    let g:buftabline_seperators = 1
endif
"}}}
" undotree {{{
if has("persistent_undo")
    set undodir=~/.undodir
    set undofile
endif
nnoremap <F9> :UndotreeToggle<CR>
" }}}
" vim-fugitive {{{
nmap g[ :diffget //3<CR>
nmap g] :diffget //2<CR>
nmap <leader>gs :Gstatus<CR>
" }}}
" vimwiki{{{
let hostname = substitute(system('hostname'), '\n', '', '')
if hostname == "pollux"
	let g:vimwiki_list = [{'path': '/pool/Sync/vimwiki/',
				\ 'syntax': 'markdown', 'ext': '.md'}]
else
	let g:vimwiki_list = [{'path': '~/Sync/vimwiki/',
				\ 'syntax': 'markdown', 'ext': '.md'}]
endif"}}}


" SCRIPTS
augroup highlight_yank
    autocmd!
    autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank({timeout = 200})
augroup END
" Make whitespace visable"{{{
    exec "set listchars=space:·,extends:>,precedes:<,tab:\uBB\uBB,trail:\uB7,nbsp:~"
"}}}
" Highlight 81st column when exceeded (256color only, disabled for now "{{{
" highlight ColorColumn ctermbg=magenta
" call matchadd('ColorColumn', '\%81v', 100)
set colorcolumn=81
hi ColorColumn guibg=#200505

"}}}
" Toggle line wrapping {{{
nnoremap - :call ToggleWrap()<CR>
function! ToggleWrap()
  if !exists("b:wrap") || !b:wrap
    call WrapOn()
    echo "Wordwrap is ON"
    let b:wrap=1
  else
    call WrapOff()
    echo "Wordwrap is OFF"
    let b:wrap=0
  endif
endfunction
function! WrapOn()
	setlocal nocursorline
    setlocal wrap
    setlocal linebreak
    nnoremap <buffer> j gj
    nnoremap <buffer> k gk
    vnoremap <buffer> j gj
    vnoremap <buffer> k gk
endfunction
function! WrapOff()
	setlocal cursorline
    setlocal nowrap
    setlocal nolinebreak
    nnoremap <buffer> j j
    nnoremap <buffer> k k
    vnoremap <buffer> j j
    vnoremap <buffer> k k
endfunction
"}}}
" Toggle the background between light and dark. {{{
function! BgToggleVar()
  if &background == "light"
    execute ":set background=dark"
  else
    execute ":set background=light"
  endif
endfunction
function! BgToggleName()
	exe "colors" (g:colors_name =~# "dark"
				\ ? substitute(g:colors_name, 'dark', 'light', '')
				\ : substitute(g:colors_name, 'light', 'dark', '')
				\ )
endfunction
"}}}

" SPECIFIC FILETYPE HANDLING
" Helper functions to dedup code {{{
function! ReadPre()
	" syntax off
	setlocal bin
	setlocal viminfo=
	setlocal noswapfile
endfunction
function! ReadPost(cmd)
  let $vimpass = inputsecret("Password: ")
  exec "silent! '[,']!" . a:cmd
  setlocal nobin
endfunction
function! WritePre(cmd)
  setlocal bin
  if empty($vimpass)
  let $vimpass = inputsecret("Password: ")
  endif
  exec "silent! '[,']!" . a:cmd
endfunction
function! WritePost()
  silent! u
  setlocal nobin
endfunction
"}}}
" scrypt - external reading/writing of .scrypt files {{{
augroup CPT
  au!
  au BufReadPre *.scrypt call ReadPre()
  au BufReadPost *.scrypt call ReadPost("scrypt dec --passphrase env:vimpass -")
  au BufWritePre *.scrypt call WritePre("scrypt enc --passphrase env:vimpass -")
  au BufWritePost *.scrypt call WritePost()
augroup END
"}}}
" mg - external reading/writing of .sc {{{
augroup MG2
  au!
  au BufReadPre *.mangle,*.mangle.md call ReadPre()
  au BufReadPost *.mangle,*.mangle.md call ReadPost("mangle dec --env vimpass")
  au BufWritePre *.mangle,*.mangle.md call WritePre("mangle enc --env vimpass")
  au BufWritePost *.mangle,*.mangle.md call WritePost()
augroup END
"}}}
