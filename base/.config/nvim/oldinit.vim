let mapleader="\<Space>"
let maplocalleader=","
" Install vim-plug if it isn't already. Needs curl"{{{
if has("nvim")
	" linux nvim
	let s:ml_path='~/.config/nvim/autoload/plug.vim'
else
	" linux vim
	let s:ml_path='~/.vim/autoload/plug.vim'
endif
if empty(glob(s:ml_path))
	exec "!curl -fLo " . s:ml_path . " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
autocmd VimEnter * PlugInstall | source $MYVIMRC
endif
"}}}
" Load Plugins with vim-plug {{{
call plug#begin('~/.config/nvim/bundle')

" #### Completion
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neovim/nvim-lspconfig'
Plug 'glepnir/lspsaga.nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update
Plug 'hrsh7th/nvim-compe'
Plug 'windwp/nvim-autopairs'
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip-integ'
Plug 'nvim-treesitter/nvim-treesitter-textobjects'
" fuzzy
Plug 'nvim-telescope/telescope.nvim'

" #### panels / bars / windows / gutters
" Plug 'ap/vim-buftabline'
" Plug 'akinsho/nvim-bufferline.lua'
Plug 'tpope/vim-commentary'			" gc{motion} or visual select and gc
" #### Apearence
Plug 'kyazdani42/nvim-web-devicons'
" Plug 'hoob3rt/lualine.nvim'
Plug 'nvim-lualine/lualine.nvim'
Plug 'gruvbox-community/gruvbox'
" #### Movement
Plug 'zirrostig/vim-schlepp'
Plug 'tpope/vim-repeat'
Plug 'rhysd/clever-f.vim'
Plug 'justinmk/vim-sneak'
Plug 'ThePrimeagen/harpoon'

" #### Manipulation
Plug 'tpope/vim-surround' 			" yssb ysiwb csb} ds}

" ### Libraries
Plug 'nvim-lua/plenary.nvim'

" #### Git
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

" #### Other
Plug 'tpope/vim-obsession'
" Plug 'rmagatti/auto-session'
Plug 'Shougo/unite.vim'
Plug 'justinmk/vim-gtfo'				" Opens terminal or filemanager
Plug 'qpkorr/vim-renamer'
Plug 'roxma/vim-tmux-clipboard'
Plug 'mbbill/undotree'
Plug 'christoomey/vim-tmux-navigator'
" Plug 'vimwiki/vimwiki'

call plug#end()
"}}}
" Color Schemes {{{

" THEMES BELOW, USE AT LEAST ONE.
if has("termguicolors")
	" hmm very much needed still
	set termguicolors
	" still needed for vim in tmux, dunno why dunno what it is, magic, hate it.
	let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
	let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
set background=dark
let g:gruvbox_invert_selection=0
colorscheme gruvbox
nnoremap <F5> :call BgToggleVar()<CR>

"}}}
" Set {{{
" [ handled by vim-plug
" filetype plugin indent on " load filetype-specific indent files
" syntax enable
" ] handled by vim-plug

" #### Visual **
set visualbell      " nvim default is nobel, vim is bellon
set number        " always show line numbers
set relativenumber
set cursorline      " highlight current line
set lazyredraw      " redraw only when we need to
set scrolloff=8
set cmdheight=2

" Buffers
set hidden
set nobackup
set noswapfile
set updatetime=300
set nowrap          " don't wrap lines
set shortmess+=c " prevent hit enter messages

" #### tabs, tabs for indentation, spaces for alignment
set noexpandtab
set tabstop=4       " a tab is four spaces
set softtabstop=4	" number of spaces <tab> counts for when editing
set shiftwidth=4    " number of spaces to use for autoindenting
set shiftround      " use multiple of shiftwidth when indenting with '<' and '>'
set pastetoggle=<F3>

" #### Search
set showmatch       " set show matching parenthesis
set ignorecase      " ignore case when searching
set smartcase       " ignore case if search pattern is all lowercase,

set spelllang=en_gb
set spellsuggest=10

set foldmethod=marker
set foldnestmax=1

set clipboard+=unnamedplus
set inccommand=nosplit

set sessionoptions+=globals
" auto-session recommended
" set sessionoptions+=terminal


" nvim defaults that we need in vim
if !has("nvim")
	set showcmd
    set wildmenu
	set encoding=utf-8
	set hlsearch        " highlight search hlsearch/nohlsearch
	set incsearch       " search as characters are entered
	set autoread
	set autoindent
	set backspace=indent,eol,start  " Makes backspace key more powerful.
	set ttyfast
endif
set nohlsearch

" fonts, multi-platform
if has("gui_running")
	set lines=40
    set columns=120
  if has("gui_gtk2") || has("gui_gtk3")
	set guifont=Noto\ Mono\ 11
  elseif has("gui_photon")
    set guifont=Courier\ New:s11
  elseif has("gui_kde")
    set guifont=Courier\ New/11/-1/5/50/0/0/0/1/0
  elseif has("x11")
    set guifont=-*-courier-medium-r-normal-*-*-180-*-*-m-*-*
  else
    set guifont=Lucida_Console:h12:cANSI"
  endif
  " ## other GUI only options
  " remove toolbars
  set guioptions-=t
  set guioptions-=T
  " remove menu
  set guioptions-=m
  " remove scroll bars
  set guioptions-=rL
  " use console confirm instead of dialog
  set guioptions+=c
  " stop cursor blinking
  set guicursor+=a:blinkon0
endif

" In ONI diable statusbar as we have a gui one.
if exists("g:gui_oni")
	set noshowmode
	set noruler
	set laststatus=0
	set noshowcmd
	set mouse=a
endif

set undofile


" }}}
" Key Remaps"{{{
nnoremap ; :
nnoremap : ;
vnoremap ; :
vnoremap : ;
nnoremap <BS> :noh<CR>
nnoremap <F8> :bd<CR>
nnoremap <leader>s :setlocal nospell!<CR>
" move through windows and tmux panes (with tmux-navigator)
nnoremap <silent> <C-k> :wincmd k<CR>
nnoremap <silent> <C-j> :wincmd j<CR>
nnoremap <silent> <C-h> :wincmd h<CR>
nnoremap <silent> <C-l> :wincmd l<CR>
imap jk <ESC>
imap <C-h> <Left>
imap <C-l> <Right>
nmap <Up> [[zz
nmap <Down> ]]zz
" make Y behave like C and D
nnoremap Y y$
" keep cursor is same place during next/prev join
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z

" buffer navigation
" now in tabline section

" this file!
nnoremap <leader>vr :source $MYVIMRC<CR>
nnoremap <leader>ve :e $MYVIMRC<CR>

" replace all
nnoremap <leader>ra yiw:%s/\<<C-r>"\>//g<left><left>
" replace all with confirm
nnoremap <leader>rc yiw:%s/\<<C-r>"\>//gc<left><left><left>
" elevate to root and save
command! W :w !sudo tee %
" easier to reach Start of text and end of line
nnoremap H ^
nnoremap L $
nnoremap M `
" no matter what we have set, we can toggle with these keys
nnoremap <bar> :setlocal cursorcolumn!<CR>
nnoremap _ :setlocal cursorline!<CR>
" faster macroing
nnoremap @ q
nnoremap q @
"}}}

" PLUGIN CONFIGURATION
if has("nvim")
" terminal {{{
inoremap <F10> <ESC>:term<CR>i
nnoremap <F10> :term<CR>i
autocmd BufWinEnter,WinEnter term://* startinsert
tnoremap jk <C-\><C-n>
tnoremap <C-j> <C-\><C-n>:wincmd j<CR>
tnoremap <C-k> <C-\><C-n>:wincmd k<CR>
tnoremap <C-h> <C-\><C-n>:wincmd h<CR>
tnoremap <C-l> <C-\><C-n>:wincmd l<CR>
tnoremap <C-n> <C-\><C-n>:bn<CR>
tnoremap <C-p> <C-\><C-n>:bp<CR>
" unmap vim-sneaks use of '\' as we need it.
autocmd VimEnter * noremap \ <NOP>
"}}}
" coc {{{
" Highlight the symbol and its references when holding the cursor.
" autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
" nmap <leader>rn <Plug>(coc-rename)
" Use `[c` and `]c` to navigate diagnostics
" nmap <silent> [c <Plug>(coc-diagnostic-prev)
" nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
" nmap <silent> gd <Plug>(coc-definition)
" nmap <silent> gy <Plug>(coc-type-definition)
" nmap <silent> gi <Plug>(coc-implementation)
" nmap <silent> gr <Plug>(coc-references)
" Use <c-space> to trigger completion.
" inoremap <silent><expr> <c-space> coc#refresh()

" ---- COC-YANK ----
" nnoremap <silent> <space>y  :<C-u>CocList -A --normal yank<cr>
" }}}
" lsp lspsaga treesitter nvim-autopairs {{{
set completeopt=menuone,preview,noselect
lua <<EOF
require'lspconfig'.gopls.setup{}
require'lspconfig'.pyright.setup{}
--Enable (broadcasting) snippet capability for completion
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
require'lspconfig'.html.setup {
  capabilities = capabilities,
}
require'lspsaga'.init_lsp_saga()
require'nvim-treesitter.configs'.setup {
ensure_installed = { "bash", "comment", "c", "cpp", "css",
"dockerfile", "gomod", "go", "html", "json", "lua", "python",
"toml", "yaml" }, -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  ignore_install = { "javascript" }, -- List of parsers to ignore installing
  highlight = {
    enable = true,              -- false will disable the whole extension
    disable = { "php" },  -- list of language that will be disabled
    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}
require('nvim-autopairs').setup()
require("nvim-autopairs.completion.compe").setup({
  map_cr = true, --  map <CR> on insert mode
  map_complete = true -- it will auto insert `(` after select function or method item
})
require'nvim-treesitter.configs'.setup {
  textobjects = {
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        ["]]"] = "@function.outer",
      },
      goto_previous_start = {
        ["[["] = "@function.outer",
      },
    },
    select = {
      enable = true,

      -- Automatically jump forward to textobj, similar to targets.vim 
      lookahead = true,

      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["ab"] = "@block.outer",
        ["ib"] = "@block.inner",

        -- Or you can define your own textobjects like this
        ["iF"] = {
          python = "(function_definition) @function",
          cpp = "(function_definition) @function",
          c = "(function_definition) @function",
          java = "(method_declaration) @function",
        },
      },
    },
  },
}
EOF
nnoremap <silent> gh :Lspsaga lsp_finder<CR>
" nnoremap <silent><leader>ca :Lspsaga code_action<CR>
nnoremap <leader>ca <cmd>lua vim.lsp.buf.code_action()<CR>
vnoremap <silent><leader>ca :<C-U>Lspsaga range_code_action<CR>
nnoremap <silent>K :Lspsaga hover_doc<CR>
nnoremap <silent> <C-f> <cmd>lua require('lspsaga.action').smart_scroll_with_saga(1)<CR>
nnoremap <silent> <C-b> <cmd>lua require('lspsaga.action').smart_scroll_with_saga(-1)<CR>
nnoremap <silent>gr :Lspsaga rename<CR>
nnoremap <silent> gd :Lspsaga preview_definition<CR>
nnoremap <silent> [e :Lspsaga diagnostic_jump_next<CR>
nnoremap <silent> ]e :Lspsaga diagnostic_jump_prev<CR>
nnoremap <silent> <leader>f :lua vim.lsp.buf.formatting()<CR>
let g:compe = {}
let g:compe.enabled = v:true
let g:compe.autocomplete = v:true
let g:compe.debug = v:false
let g:compe.min_length = 1
let g:compe.preselect = 'enable'
let g:compe.throttle_time = 80
let g:compe.source_timeout = 200
let g:compe.resolve_timeout = 800
let g:compe.incomplete_delay = 400
let g:compe.max_abbr_width = 100
let g:compe.max_kind_width = 100
let g:compe.max_menu_width = 100
let g:compe.documentation = v:true

let g:compe.source = {}
let g:compe.source.path = v:true
let g:compe.source.buffer = v:true
let g:compe.source.calc = v:true
let g:compe.source.nvim_lsp = v:true
let g:compe.source.nvim_lua = v:true
let g:compe.source.vsnip = v:true
let g:compe.source.ultisnips = v:true
let g:compe.source.luasnip = v:true
let g:compe.source.emoji = v:true
" }}}
" Find files using Telescope command-line sugar.{{{
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>"}}}
" Harpoon {{{
lua << EOF
require("harpoon").setup()
EOF
nnoremap ga :lua require("harpoon.mark").add_file()<CR>
nnoremap g1 :lua require("harpoon.ui").nav_file(1)<CR>
nnoremap g2 :lua require("harpoon.ui").nav_file(2)<CR>
nnoremap g3 :lua require("harpoon.ui").nav_file(3)<CR>
nnoremap g4 :lua require("harpoon.ui").nav_file(4)<CR>
nnoremap g5 :lua require("harpoon.ui").nav_file(5)<CR>
nnoremap gm :lua require("harpoon.ui").toggle_quick_menu()<CR>
" }}}
else
" neocomplete {{{
	" disable AutoComplPop
	let g:acp_enableAtStartup = 0
	" Use neocomplete
    let g:neocomplete#enable_at_startup = 1
	let g:neocomplete#sources#go#gocode_binary = '/home/mark/bin/gocode'
"}}}
endif
" vim-sneak {{{
let g:sneak#label = 1
"}}}
" gtfo  got(terminal) gof(file explorer) {{{
let g:gtfo#terminals = { 'unix': 'alacritty --working-directory $pwd' }
" let g:gtfo#terminals = { 'unix': 'konsole --new-tab --workdir' }
" }}}
" schlepp, moves visual blocks {{{
vmap <up>    <Plug>SchleppUp
vmap <down>  <Plug>SchleppDown
vmap <left>  <Plug>SchleppLeft
vmap <right> <Plug>SchleppRight
"}}}
" Tabline and Statusline {{{ 
lua << EOF
require('lualine').setup { sections = {
		lualine_z = {'mode'},
		lualine_x = {'fileformat','encoding'},
		lualine_y = {'location'}
	},
	tabline = {
		lualine_a = {'buffers'},
		lualine_z = {'tabs'}
	}
}
-- require("bufferline").setup{
--	options = {
--		numbers = "none",
--		show_close_icon = false,
--		show_buffer_close_icons = false,
--		separator_style = "slant",
--		always_show_bufferline = true,
--		diagnostics = "nvim_lsp",
--		sort_by = 'extension'
--		}
--}
EOF
" # Buffer Navigation #
" normal maps
nnoremap <C-n> :bn<CR>
nnoremap <C-p> :bp<CR>
inoremap <C-n> <ESC>:bn<CR>
inoremap <C-p> <ESC>:bp<CR>
" bufferline binds, because reordering breaks inbuilt
" nnoremap <silent><C-n> :BufferLineCycleNext<CR>
" nnoremap <silent><C-p> :BufferLineCyclePrev<CR>
" inoremap <silent><C-n> <ESC>:BufferLineCycleNext<CR>
" inoremap <silent><C-p> <ESC>:BufferLineCyclePrev<CR>
" These commands will move the current buffer backwards or forwards in the bufferline
" nnoremap <silent>gbl :BufferLineMoveNext<CR>
" nnoremap <silent>gbh :BufferLineMovePrev<CR>

" replaces all bars with my own status lines, not pretty, but MINE.
" if !exists("g:gui_oni")
"     au InsertEnter * call SaveStatusLine()
"     au InsertLeave * call RestoreStatusLine()
"     set laststatus=2
"     set statusline=
"     set statusline+=%f%m%r%h%w
"     set statusline+=%{fugitive#statusline()}      " [Git(master)]
"     set statusline+=%=                            " right align
"     set statusline+=%y
"     set statusline+=[%{&fileformat}]               " file format
"     set statusline+=[%{strlen(&fenc)?&fenc:&enc}] " encoding
"     set statusline+=\ %p%%\ C:%v
"     function! SaveStatusLine()
"         let g:slBg = synIDattr(hlID("StatusLine"), "bg#")
"         let g:slFg = synIDattr(hlID("StatusLine"), "fg#")
"         let r = synIDattr(hlID("StatusLine"), "reverse")
"         let i = synIDattr(hlID("StatusLine"), "inverse")
"         if r==1 || i==1 " handle themes that use reverse
"             hi StatusLine guifg=#33AA44 guibg=#000000
"         else
"             hi StatusLine guibg=#33AA44 guifg=#000000
"         endif
"         return ''
"     endfunction
"     function! RestoreStatusLine()
"         exec 'hi StatusLine guibg=' . g:slBg . ' guifg=' . g:slFg
"     endfunction
"     function! LightLineFugitive()
"         return exists('*fugitive#head') ? fugitive#head() : ''
"     endfunction
"     let g:buftabline_indicators = 1
"     let g:buftabline_seperators = 1
" endif
"}}}
" undotree {{{
if has("persistent_undo")
    set undodir=~/.undodir
    set undofile
endif
nnoremap <F9> :UndotreeToggle<CR>
" }}}
" vim-fugitive {{{
nmap g[ :diffget //3<CR>
nmap g] :diffget //2<CR>
nmap <leader>gs :Gstatus<CR>
" }}}
" vimwiki{{{
let hostname = substitute(system('hostname'), '\n', '', '')
if hostname == "pollux"
	let g:vimwiki_list = [{'path': '/pool/Sync/vimwiki/',
				\ 'syntax': 'markdown', 'ext': '.md'}]
else
	let g:vimwiki_list = [{'path': '~/Sync/vimwiki/',
				\ 'syntax': 'markdown', 'ext': '.md'}]
endif
" wiki.vim
let g:wiki_root = '~/Sync/wiki'

"}}}


" SCRIPTS
augroup highlight_yank
    autocmd!
    autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank({timeout = 200})
augroup END
" Make whitespace visable"{{{
    exec "set listchars=space:·,extends:>,precedes:<,tab:\uBB\uBB,trail:\uB7,nbsp:~"
"}}}
" Highlight 81st column when exceeded (256color only, disabled for now "{{{
" highlight ColorColumn ctermbg=magenta
" call matchadd('ColorColumn', '\%81v', 100)
set colorcolumn=81
hi ColorColumn guibg=#200505

"}}}
" Toggle line wrapping {{{
nnoremap - :call ToggleWrap()<CR>
function! ToggleWrap()
  if !exists("b:wrap") || !b:wrap
    call WrapOn()
    echo "Wordwrap is ON"
    let b:wrap=1
  else
    call WrapOff()
    echo "Wordwrap is OFF"
    let b:wrap=0
  endif
endfunction
function! WrapOn()
	setlocal nocursorline
    setlocal wrap
    setlocal linebreak
    nnoremap <buffer> j gj
    nnoremap <buffer> k gk
    vnoremap <buffer> j gj
    vnoremap <buffer> k gk
endfunction
function! WrapOff()
	setlocal cursorline
    setlocal nowrap
    setlocal nolinebreak
    nnoremap <buffer> j j
    nnoremap <buffer> k k
    vnoremap <buffer> j j
    vnoremap <buffer> k k
endfunction
"}}}
" Toggle the background between light and dark. {{{
function! BgToggleVar()
  if &background == "light"
    execute ":set background=dark"
  else
    execute ":set background=light"
  endif
endfunction
function! BgToggleName()
	exe "colors" (g:colors_name =~# "dark"
				\ ? substitute(g:colors_name, 'dark', 'light', '')
				\ : substitute(g:colors_name, 'light', 'dark', '')
				\ )
endfunction
"}}}

" SPECIFIC FILETYPE HANDLING
" Helper functions to dedup code {{{
function! ReadPre()
	" syntax off
	setlocal bin
	setlocal viminfo=
	setlocal noswapfile
endfunction
function! ReadPost(cmd)
  let $vimpass = inputsecret("Password: ")
  exec "silent! '[,']!" . a:cmd
  setlocal nobin
endfunction
function! WritePre(cmd)
  setlocal bin
  if empty($vimpass)
  let $vimpass = inputsecret("Password: ")
  endif
  exec "silent! '[,']!" . a:cmd
endfunction
function! WritePost()
  silent! u
  setlocal nobin
endfunction
"}}}
" scrypt - external reading/writing of .scrypt files {{{
augroup CPT
  au!
  au BufReadPre *.scrypt call ReadPre()
  au BufReadPost *.scrypt call ReadPost("scrypt dec --passphrase env:vimpass -")
  au BufWritePre *.scrypt call WritePre("scrypt enc --passphrase env:vimpass -")
  au BufWritePost *.scrypt call WritePost()
augroup END
"}}}
" mg - external reading/writing of .sc {{{
augroup MG2
  au!
  au BufReadPre *.mangle,*.mangle.md call ReadPre()
  au BufReadPost *.mangle,*.mangle.md call ReadPost("mangle dec --env vimpass")
  au BufWritePre *.mangle,*.mangle.md call WritePre("mangle enc --env vimpass")
  au BufWritePost *.mangle,*.mangle.md call WritePost()
augroup END
"}}}
