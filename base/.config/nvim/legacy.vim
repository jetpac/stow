
augroup highlight_yank
    autocmd!
    autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank({timeout = 200})
augroup END
augroup vimrc-incsearch-highlight
	autocmd!
	autocmd CmdlineEnter /,\? :set hlsearch
	autocmd CmdlineLeave /,\? :set nohlsearch
augroup END

" SPECIFIC FILETYPE HANDLING
" Helper functions to dedup code {{{
function! ReadPre()
	" syntax off
	setlocal bin
	setlocal viminfo=
	setlocal noswapfile
endfunction
function! ReadPost(cmd)
  let $vimpass = inputsecret("Password: ")
  exec "silent! '[,']!" . a:cmd
  setlocal nobin
endfunction
function! WritePre(cmd)
  setlocal bin
  if empty($vimpass)
  let $vimpass = inputsecret("Password: ")
  endif
  exec "silent! '[,']!" . a:cmd
endfunction
function! WritePost()
  silent! u
  setlocal nobin
endfunction
"}}}
" scrypt - external reading/writing of .scrypt files {{{
augroup CPT
  au!
  au BufReadPre *.scrypt call ReadPre()
  au BufReadPost *.scrypt call ReadPost("scrypt dec --passphrase env:vimpass -")
  au BufWritePre *.scrypt call WritePre("scrypt enc --passphrase env:vimpass -")
  au BufWritePost *.scrypt call WritePost()
augroup END
"}}}
" mg - external reading/writing of .sc {{{
augroup MG2
  au!
  au BufReadPre *.mangle,*.mangle.md call ReadPre()
  au BufReadPost *.mangle,*.mangle.md call ReadPost("mangle dec --env vimpass")
  au BufWritePre *.mangle,*.mangle.md call WritePre("mangle enc --env vimpass")
  au BufWritePost *.mangle,*.mangle.md call WritePost()
augroup END
"}}}
