require('configs.nvim-treesitter')
require('configs.nvim-cmp')
require('configs.lspconfig') -- after nvim-cmp
require('configs.lualine')
require('configs.nvim-bufferline')
require('gitsigns').setup()

require('nvim-autopairs').setup{}
require('telescope').setup{}
-- legacy setup{{{
vim.g['sneak#label'] = 1
vim.g.wiki_filetypes = {'md'}
vim.g.wiki_link_extension = '.md'
vim.g.wiki_link_target_type = 'md'
if vim.fn.hostname() == 'pollux' then
	vim.g.wiki_root = '/pool/Sync/wiki'
else
	vim.g.wiki_root = '~/Sync/wiki'
end
--}}}

