-- tabs, tabs for indentation, spaces for alignment. use ftplugins to overrule
vim.opt.expandtab = false
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.shiftround = true
vim.opt.pastetoggle="<F3>"

-- lines
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.scrolloff = 8
vim.opt.shortmess:append("c")
vim.opt.wrap = false
-- set cmdheight=2

-- Searching
vim.opt.showmatch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.hlsearch = false

-- spelling
vim.opt.spelllang = 'en_gb'
vim.opt.spellsuggest = '10'
-- folding
vim.opt.foldmethod = 'marker'
vim.opt.foldnestmax = 1

vim.o.clipboard = "unnamedplus"
-- vim.opt.sessionoptions:append('options')
vim.o.sessionoptions="blank,buffers,curdir,folds,help,tabpages,winsize,winpos,terminal"
vim.opt.completeopt = "menu,menuone,noselect"
-- vim.opt.undofile = true
-- Appearance
vim.opt.background = "dark" -- or "light" for light mode
vim.cmd([[colorscheme gruvbox]])
vim.opt.termguicolors = true
vim.opt.listchars:append({ tab = "<->", extends = ">", precedes = "<", space="∙"})
vim.opt.list = false
vim.opt.colorcolumn = {'81','121'}
vim.cmd([[hi ColorColumn guibg=#200505]])
