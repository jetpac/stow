require("bufferline").setup{
	options = {
		numbers = "none",
		show_close_icon = false,
		show_buffer_close_icons = false,
		separator_style = "slant",
		always_show_bufferline = true,
		diagnostics = "nvim_lsp",
		sort_by = 'id'
	}
}

