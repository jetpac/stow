require'lspsaga'.init_lsp_saga()

nmap("gh", ":Lspsaga lsp_finder<CR>")
-- nmap("<leader>ca", ":Lspsaga code_action<CR>")
nmap("<leader>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>")
-- vnoremap <silent><leader>ca :<C-U>Lspsaga range_code_action<CR>
-- K used by up page nmap("K", ":Lspsaga hover_doc<CR>")
nmap("<C-f>", "<cmd>lua require('lspsaga.action').smart_scroll_with_saga(1)<CR>")
nmap("<C-b>", "<cmd>lua require('lspsaga.action').smart_scroll_with_saga(-1)<CR>")
nmap("gr", ":Lspsaga rename<CR>")
nmap("gd", ":Lspsaga preview_definition<CR>")
nmap("[e", ":Lspsaga diagnostic_jump_next<CR>")
nmap("]e", ":Lspsaga diagnostic_jump_prev<CR>")
nmap("<leader>f", ":lua vim.lsp.buf.formatting()<CR>")


