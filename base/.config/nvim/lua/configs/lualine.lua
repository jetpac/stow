require('lualine').setup {
	options = {
		theme = 'gruvbox',
		section_separators = { left = '', right = ''},
		component_separators = { left = '', right = ''}
	},
	sections = {
		lualine_z = {'mode'},
		lualine_x = {'fileformat','encoding'},
		lualine_y = {'location'}
	},
	-- tabline = {
	-- 	lualine_a = {'buffers'},
	-- 	lualine_b = {},
	-- 	lualine_c = {},
	-- 	lualine_x = {},
	-- 	lualine_y = {},
	-- 	lualine_z = {'tabs'}
	-- }
}

