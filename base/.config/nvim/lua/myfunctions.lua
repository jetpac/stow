
-- ToggleWrap() toggle linewrap and remap j/k keys for handling wrapped lines {{{
function ToggleWrap()
	if vim.wo.wrap == nil or vim.wo.wrap == false then
		vim.wo.wrap = true
		vim.wo.cursorline = false -- cursorline highlight doesnt make sense on wrapped line.
		vim.wo.linebreak = true		-- wrap in spaces only, not in middle of words
		vim.api.nvim_buf_set_keymap(0, "n", "j", "gj", {noremap = true})
		vim.api.nvim_buf_set_keymap(0, "n", "k", "gk", {noremap = true})
		vim.api.nvim_buf_set_keymap(0, "v", "j", "gj", {noremap = true})
		vim.api.nvim_buf_set_keymap(0, "v", "k", "gk", {noremap = true})
	else
		vim.wo.wrap = false
		vim.wo.cursorline = true
		vim.wo.linebreak = false
		vim.api.nvim_buf_set_keymap(0, "n", "j", "j", {noremap = true})
		vim.api.nvim_buf_set_keymap(0, "n", "k", "k", {noremap = true})
		vim.api.nvim_buf_set_keymap(0, "v", "j", "j", {noremap = true})
		vim.api.nvim_buf_set_keymap(0, "v", "k", "k", {noremap = true})
	end
end
-- }}}

-- ToggleBackground() toggles background between 'light' and 'dark'{{{
function ToggleBackground()
  if vim.o.background == "light" then
    vim.o.background = 'dark'
  else
    vim.o.background = 'light'
  end
end
--}}}



