local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

function get_setup(name)
	return string.format('require("setup/%s")', name)
end

return require('packer').startup(function(use)
	-- Manage Self
	use 'wbthomason/packer.nvim'

	-- Apearance
	use 'gruvbox-community/gruvbox'
	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'kyazdani42/nvim-web-devicons', opt = true }
	}
	use {
		-- 'akinsho/nvim-bufferline.lua',
		'akinsho/bufferline.nvim',
		requires = { 'kyazdani42/nvim-web-devicons', opt = true }
	}

	-- Navigation
	if (vim.loop.os_gethostname() == 'pollux')
	then
		use 'christoomey/vim-tmux-navigator'
		-- use {
		-- 	'christoomey/vim-tmux-navigator',
		-- 	config = function()
		-- 		require('vim-tmux-navigator').setup()
		-- 	end
		-- }
	else
		use {
			'hermitmaster/nvim-kitty-navigator',
			run='cp kitty/* ~/.config/kitty',
			config = function()
				require('nvim-kitty-navigator').setup()
			end
		}
	end
	-- use 'justinmk/vim-sneak'
	use 'ggandor/lightspeed.nvim'
	-- git
	use 'tpope/vim-fugitive'
	use {
		'lewis6991/gitsigns.nvim',
		requires = {
			'nvim-lua/plenary.nvim'
		}
		-- tag = 'release' -- To use the latest release
	}
	-- helpers
	use 'windwp/nvim-autopairs'
	-- use 'tpope/vim-obsession'
	use {
	'rmagatti/auto-session',
	config = function()
		require('auto-session').setup {
		log_level = 'info',
		auto_session_suppress_dirs = {'~/', '~/Projects'}
		}
	end
	}

	-- LSP
	use 'williamboman/nvim-lsp-installer'
	use 'neovim/nvim-lspconfig'
	use {'nvim-treesitter/nvim-treesitter', run=':TSUpdate'}
	use 'nvim-treesitter/nvim-treesitter-textobjects'
	-- Completion
	use 'hrsh7th/cmp-nvim-lsp'
	use 'hrsh7th/cmp-buffer'
	use 'hrsh7th/cmp-path'
	use 'hrsh7th/nvim-cmp'
	-- For vsnip users.
	use 'hrsh7th/cmp-vsnip'
	use 'hrsh7th/vim-vsnip'

	-- files
	use {
		'nvim-telescope/telescope.nvim',
		requires = { {'nvim-lua/plenary.nvim'} }
	}
	-- Organisation
	use 'lervag/wiki.vim'
	-- Manipulation
	use 'tpope/vim-commentary';			-- gcc / g
	use 'tpope/vim-surround'; 			-- yssb ysiwb csb} ds
	use 'qpkorr/vim-renamer'

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)

