-- mapping functions
function map(mode, lhs, rhs, opts)
    local options = { noremap = true }
    if opts then
        options = vim.tbl_extend("force", options, opts)
    end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- function nmap(shortcut, command)
--     map('n', shortcut, command)
-- end

-- function imap(shortcut, command)
--     map('i', shortcut, command)
-- end

-- function vmap(shortcut, command)
--     map('v', shortcut, command)
-- end

map("n", ";", ":")
map("n", ":", ";")
map("v", ";", ":")
map("n", "<F8>", ":bd<CR>")
map("n", "<leader>s", ":setlocal nospell!<CR>")

-- buffer navigation
map("n", "<C-n>", ":bn<CR>")
map("n", "<C-p>", ":bp<CR>")
map("i", "<C-n>", "<ESC>:bn<CR>")
map("i", "<C-p>", "<ESC>:bp<CR>")

-- tmux navigator
-- vim.api.nvim_set_keymap("n", "<C-h>", ":wincmd h<CR>", {noremap = true, silent = true})
map("n", "<C-h>", ":wincmd h<CR>", {silent = true})
map("n", "<C-j>", ":wincmd j<CR>", {silent = true})
map("n", "<C-k>", ":wincmd k<CR>", {silent = true})
map("n", "<C-l>", ":wincmd l<CR>", {silent = true})

-- toggle line wrap
-- nmap("-", ":call ToggleWrap()<CR>")
map("n", "-", ":lua ToggleWrap()<CR>")

-- my preferences
map("n", "<F5>", ":lua ToggleBackground()<CR>")
map("i", "<C-g>", "<C-k>")
map("i", "jk", "<ESC>")
map("i", "<C-h>", "<Left>")
map("i", "<C-l>", "<Right>")
map("i", "<C-k>", "<Up>")
map("i", "<C-j>", "<Down>")
map("n", "K", "<C-u>")
map("n", "J", "<C-d>")
map("n", "H", "^")
map("n", "L", "$")
map("n", "<Up>", "[[zz")
map("n", "<Down>", "]]zz")
-- make Y behave like C and D
map("n", "Y", "y$")
-- keep cursor is same place during next/prev join
map("n", "n", "nzzzv")
map("n", "N", "Nzzzv")
-- no good mapping, J taken vim.api.nvim_set_keymap("n", "J", "mzJ`z", {noremap = true})
map("n", "M", "m")
map("n", "m", "`")

-- this file!
map("n", "<leader>vr", ":luafile $MYVIMRC<CR>")
map("n", "<leader>ve", ":e ~/.config/nvim/lua<CR>")

-- replace all / all with confirm
vim.api.nvim_set_keymap("n", "<leader>ra", 'yiw:%s/<C-r>"//g<left><left>', {noremap = true})
vim.api.nvim_set_keymap("n", "<leader>rc", 'yiw:%s/<C-r>"//gc<left><left><left>', {noremap = true})


