#

# functions{{{
function xcd(){
	cd $(qcd $1)
}

# add to path, but only if directory exists AND not alreay in path
function pathmunge () {
	if [ ! -d "$1" ] ; then
		return 0
	fi
	if ! echo "$PATH" | /bin/grep -Eq "(^|:)$1($|:)" ; then
		if [ "$2" = "after" ] ; then
			PATH="$PATH:$1"
		else
			PATH="$1:$PATH"
		fi
	fi
}

function dcR() {
	docker stop $1
	docker rm $1
	docker-compose up -d $1
}


#}}}

# Aliases Common {{{
alias ls='ls --color'
alias ll='ls -lh'
alias la='ls -lAh'

# get rid of command not found ##
alias cd..='cd ..'

# a quick way to get out of current directory ##
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'

# copy with progress bar
# alias cp='rsync --progress -ah'
# vim-obsession
alias v='nvim -S'
alias e='nvim'
alias dc='docker-compose'
alias dcr='docker-compose restart'
alias tmux='env TERM=screen-256color tmux'
alias icat="kitty +kitten icat"
alias idiff="kitty +kitten diff"
if [ -f ~/.local/bin/git-prompt.sh ]; then
  source ~/.local/bin/git-prompt.sh
fi
alias gst='git status -s'
alias gls='git log --oneline --decorate'
alias glog='git log'
alias gco='git checkout'
alias gsw='git switch'
alias gbr='git branch'
alias gdiff='git diff'
alias rmount='sshfs -o reconnect,ServerAliveInterval=5,ServerAliveCountMax=1'
# }}}

# Colors and Prompt {{{
if [ $USER == "root" ]; then
	colUser="\e[38;5;196m"; # root
else
	colUser="\e[38;5;244m"; # normal user
fi

if [ $HOSTNAME == "pollux" ]; then
	colAt="\e[38;5;34m";
	colHost="\e[38;5;34m";
else
	colAt="\e[38;5;125m";
	colHost="\e[38;5;126m";
fi
reset="\e[0m";
# rest
colColon="\e[38;5;33m";
colDir="\e[38;5;33m";
colGit="\e[38;5;129m";
PS1="\[${colUser}\]\u"; # username
PS1+="\[${colAt}\]@";
PS1+="\[${colHost}\]\h"; # host
PS1+="\[${colColon}\]:";
PS1+="\[${colDir}\]\W"; # directory
PS1+="\[${colGit}\]\$(__git_ps1 '(%s)')"; 
PS1+="\[${reset}\]$ ";
export PS1;
# }}}

# $PATH {{{
# Personal scripts
pathmunge $HOME/bin after

# non-common paths
if [ $HOSTNAME == "pollux" ]; then
	# export PATH=$PATH:/pool/bin
	pathmunge /pool/bin
else
	export GOPATH=$HOME/go
	export GOPRIVATE=ld50.net
	# export PATH=$PATH:$GOPATH/bin
	# export PATH=$PATH:/usr/local/go/bin
	pathmunge /usr/local/go/bin 
	pathmunge $GOPATH/bin after
	pathmunge $HOME/.local/bin
fi

export PATH

# }}}

# Other exports {{{
# Set our editor of choice here
export EDITOR=/usr/local/bin/nvim
export NVIM_LOG_FILE=/tmp/nvimlog

# }}}

# fzf
# -- Arch location
if [[ -s /usr/share/fzf/key-bindings.bash ]] ; then
	. /usr/share/fzf/key-bindings.bash
fi
# -- Ubuntu location
if [[ -s /usr/share/doc/fzf/examples/key-bindings.bash ]] ; then
	. /usr/share/doc/fzf/examples/key-bindings.bash
fi
# git completions
if [[ -s /usr/share/git/completion/git-completion.bash ]]; then
  . /usr/share/git/completion/git-completion.bash
fi
# bash completions
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion


# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
